import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:mama_kormit/resources/resources.dart';

void main() {
  test('svgs assets test', () {
    expect(File(Svgs.heart).existsSync(), isTrue);
  });
}
