import 'dart:io';

import 'package:flutter_test/flutter_test.dart';
import 'package:mama_kormit/resources/resources.dart';

void main() {
  test('images assets test', () {
    expect(File(Images.arrowRightCircle).existsSync(), isTrue);
    expect(File(Images.bgpattern).existsSync(), isTrue);
    expect(File(Images.bonusBg).existsSync(), isTrue);
    expect(File(Images.buy).existsSync(), isTrue);
    expect(File(Images.heart).existsSync(), isTrue);
    expect(File(Images.logo).existsSync(), isTrue);
    expect(File(Images.search).existsSync(), isTrue);
  });
}
