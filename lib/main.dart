import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mama_kormit/presentation/widgets/sharedpref_widget.dart';
import 'package:mama_kormit/presentation/widgets/unfocus_textfield.dart';
import 'package:mama_kormit/routes/app_router.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return UnfocusTextField(
      child: SharedPrefWidget(
        child: MaterialApp.router(
          routerConfig: AppRouter().config(),
          debugShowCheckedModeBanner: false,
          title: 'First Method',
          // You can use the library anywhere in the app even in theme
          theme: ThemeData(
              primarySwatch: Colors.blue,
              fontFamily: GoogleFonts.inter().fontFamily),
        ),
      ),
    );
  }
}
