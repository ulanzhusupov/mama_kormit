import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:mama_kormit/presentation/screens/auth/login_screen.dart';
import 'package:mama_kormit/presentation/screens/auth/pincode_screen.dart';
import 'package:mama_kormit/presentation/screens/auth/registration_screen.dart';
import 'package:mama_kormit/presentation/screens/home/home_screen.dart';

part 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(page: LoginRoute.page),
        AutoRoute(
          page: RegistrationRoute.page,
        ),
        AutoRoute(page: PinCodeRoute.page),
        AutoRoute(
          page: HomeRoute.page,
          initial: true,
        ),
      ];
}
