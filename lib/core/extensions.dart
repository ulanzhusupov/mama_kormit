import 'package:mama_kormit/core/app_consts.dart';

extension CustomScreenUtils on double {
  double widthPercent() {
    return this / AppConsts.scrWidth;
  }

  double heightPercent() {
    return this / AppConsts.scrHeight;
  }
}
