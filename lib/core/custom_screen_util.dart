import 'package:flutter/material.dart';
import 'package:mama_kormit/core/extensions.dart';

class CustomScreenUtil {
  static double width(BuildContext context, double width) {
    return MediaQuery.of(context).size.width * width.widthPercent();
  }

  static double height(BuildContext context, double height) {
    return MediaQuery.of(context).size.height * height.heightPercent();
  }
}
