class ScreenSize {
  final double width;
  final double height;
  ScreenSize(this.width, this.height);
}
