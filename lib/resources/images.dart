part of 'resources.dart';

class Images {
  Images._();

  static const String arrowRightCircle = 'assets/png/arrow_right_circle.png';
  static const String bgpattern = 'assets/png/bgpattern.png';
  static const String bonusBg = 'assets/png/bonus_bg.png';
  static const String buy = 'assets/png/buy.png';
  static const String heart = 'assets/png/heart.png';
  static const String logo = 'assets/png/logo.png';
  static const String search = 'assets/png/search.png';
}
