import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:mama_kormit/core/custom_screen_util.dart';
import 'package:mama_kormit/core/extensions.dart';
import 'package:mama_kormit/presentation/theme/app_colors.dart';
import 'package:mama_kormit/presentation/theme/app_fonts.dart';
import 'package:mama_kormit/presentation/widgets/bonus_program_card.dart';
import 'package:mama_kormit/presentation/widgets/mamakormit_appbar.dart';
import 'package:mama_kormit/presentation/widgets/news_tile.dart';
import 'package:mama_kormit/presentation/widgets/street_location.dart';
import 'package:mama_kormit/presentation/widgets/title_section.dart';
import 'package:mama_kormit/resources/resources.dart';

@RoutePage()
class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              const MamaKormitAppBar(),
              SingleChildScrollView(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  children: [
                    SizedBox(height: CustomScreenUtil.height(context, 22)),
                    const StreetLocation(),
                    SizedBox(height: CustomScreenUtil.height(context, 32)),
                    const BonusProgramCard(),
                    SizedBox(height: CustomScreenUtil.height(context, 43)),
                    const TitleAndViewAllSection(title: "Новости"),
                    SizedBox(height: CustomScreenUtil.height(context, 17)),
                    const NewsTile(),
                    SizedBox(height: CustomScreenUtil.height(context, 40)),
                    const TitleAndViewAllSection(title: "Акции"),
                    SizedBox(height: CustomScreenUtil.height(context, 17)),
                    Container(
                      width: CustomScreenUtil.width(context, 158),
                      height: CustomScreenUtil.height(context, 219),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: AppColors.white,
                        boxShadow: const [
                          BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.1),
                            blurRadius: 40,
                          ),
                        ],
                      ),
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Container(
                                height: CustomScreenUtil.height(context, 104),
                                decoration: const BoxDecoration(
                                  color: AppColors.accentRed,
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(8),
                                    topRight: Radius.circular(8),
                                  ),
                                ),
                              ),
                              Positioned(
                                top: 10,
                                left: 9.5,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: AppColors.yellowDiscount,
                                    borderRadius: BorderRadius.circular(100),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal:
                                          CustomScreenUtil.width(context, 11),
                                      vertical: 0,
                                    ),
                                    child: Text(
                                      "-28%",
                                      style: AppFonts.s11W400.copyWith(
                                          color: AppColors.searchText),
                                    ),
                                  ),
                                ),
                              ),
                              Positioned(
                                bottom: 0,
                                right: 0,
                                child: InkWell(
                                  child: Container(
                                    decoration: const BoxDecoration(
                                      color: AppColors.accentGreen,
                                      borderRadius: BorderRadius.only(
                                        bottomRight: Radius.circular(8),
                                      ),
                                    ),
                                    child: Image.asset(
                                      Images.buy,
                                      width: 24,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: CustomScreenUtil.height(context, 41)),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
