import 'dart:math';

import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mama_kormit/core/app_consts.dart';
import 'package:mama_kormit/presentation/theme/app_colors.dart';
import 'package:mama_kormit/presentation/theme/app_fonts.dart';
import 'package:mama_kormit/presentation/widgets/app_textfield.dart';
import 'package:mama_kormit/presentation/widgets/sharedpref_widget.dart';
import 'package:mama_kormit/resources/resources.dart';
import 'package:mama_kormit/routes/app_router.dart';

@RoutePage()
class RegistrationScreen extends StatelessWidget {
  const RegistrationScreen({super.key});

  void saveNameAndPhone(String name, String phone) async {
    await SharedPrefWidget.prefs.setString(AppConsts.name, name);
    await SharedPrefWidget.prefs.setString(AppConsts.phoneNumber, phone);
  }

  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController phoneController = TextEditingController();
    return Scaffold(
      body: SafeArea(
        child: Container(
          color: AppColors.white,
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.97,
              child: Column(
                children: [
                  SizedBox(height: 121.h),
                  Image(
                    image: const AssetImage(Images.logo),
                    width: 307.w,
                  ),
                  SizedBox(height: 97.h),
                  const Text("Регистрация", style: AppFonts.s18W600),
                  SizedBox(height: 32.h),
                  AppTextField(
                    labelText: "Имя:",
                    hintText: "Введите имя",
                    controller: nameController,
                  ),
                  SizedBox(height: 30.h),
                  AppTextField(
                    labelText: "Телефон:",
                    hintText: "+7",
                    textInputType: TextInputType.phone,
                    controller: phoneController,
                  ),
                  const Spacer(),
                  SizedBox(
                    width: double.infinity,
                    height: 47.h,
                    child: ElevatedButton(
                      onPressed: () {
                        saveNameAndPhone(
                          nameController.text,
                          phoneController.text,
                        );
                        int randCode = Random().nextInt(8999) + 1000;
                        ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text(randCode.toString())));
                        context.router.push(PinCodeRoute(pinCode: randCode));
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: AppColors.accentGreen,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8),
                        ),
                      ),
                      child: const Text(
                        "Далее",
                        style: AppFonts.s14W500,
                      ),
                    ),
                  ),
                  SizedBox(height: 20.h),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
