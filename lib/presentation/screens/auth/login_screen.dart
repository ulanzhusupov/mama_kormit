import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mama_kormit/presentation/screens/auth/registration_screen.dart';
import 'package:mama_kormit/presentation/theme/app_colors.dart';
import 'package:mama_kormit/presentation/theme/app_fonts.dart';
import 'package:mama_kormit/presentation/widgets/app_textfield.dart';
import 'package:mama_kormit/resources/resources.dart';
import 'package:mama_kormit/routes/app_router.dart';

@RoutePage()
class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    TextEditingController numberController = TextEditingController();

    return Scaffold(
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height * 1,
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                Images.bgpattern,
              ),
              scale: 3,
              opacity: 0.1,
              repeat: ImageRepeat.repeat,
            ),
          ),
          child: SingleChildScrollView(
            child: Center(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Column(
                  children: [
                    SizedBox(height: 121.h),
                    Image(
                      image: const AssetImage(Images.logo),
                      width: 307.w,
                    ),
                    SizedBox(height: 97.h),
                    const Text("Вход", style: AppFonts.s18W600),
                    SizedBox(height: 32.h),
                    AppTextField(
                      labelText: "Телефон:",
                      hintText: "+996 XXX XX XX XX",
                      textInputType: TextInputType.phone,
                      controller: numberController,
                    ),
                    SizedBox(height: 20.h),
                    SizedBox(
                      width: double.infinity,
                      height: 47.h,
                      child: ElevatedButton(
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                          backgroundColor: AppColors.accentGreen,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                          ),
                        ),
                        child: const Text(
                          "Войти",
                          style: AppFonts.s14W500,
                        ),
                      ),
                    ),
                    SizedBox(height: 20.h),
                    SizedBox(
                      width: double.infinity,
                      height: 47.h,
                      child: ElevatedButton(
                        onPressed: () {
                          context.router.push(const RegistrationRoute());
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: AppColors.accentRed,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8),
                          ),
                        ),
                        child: const Text(
                          "Регистрация",
                          style: AppFonts.s14W500,
                        ),
                      ),
                    ),
                    SizedBox(height: 20.h),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
