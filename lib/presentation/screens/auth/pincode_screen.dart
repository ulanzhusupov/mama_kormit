import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mama_kormit/presentation/theme/app_colors.dart';
import 'package:mama_kormit/presentation/theme/app_fonts.dart';
import 'package:mama_kormit/resources/resources.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

@RoutePage()
class PinCodeScreen extends StatelessWidget {
  final int pinCode;

  const PinCodeScreen({super.key, required this.pinCode});

  @override
  Widget build(BuildContext context) {
    TextEditingController pincodeController = TextEditingController();

    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            const SizedBox(height: 121),
            const SizedBox(
              width: 407,
              child: Image(
                image: AssetImage(Images.logo),
                // width: 307,
              ),
            ),
            const SizedBox(height: 97),
            const Text("Регистрация", style: AppFonts.s18W600),
            const SizedBox(height: 30),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Введите код из СМС:",
                  style: AppFonts.s14W500.copyWith(color: AppColors.accentRed),
                ),
                const SizedBox(height: 5),
                SizedBox(
                  width: 209,
                  child: PinCodeTextField(
                    keyboardType: TextInputType.number,
                    length: 4,
                    appContext: context,
                    animationType: AnimationType.fade,
                    autoDismissKeyboard: true,
                    pinTheme: PinTheme(
                      shape: PinCodeFieldShape.box,
                      borderWidth: 1,
                      activeBorderWidth: 1,
                      selectedBorderWidth: 1,
                      inactiveBorderWidth: 1,
                      disabledBorderWidth: 1,
                      errorBorderWidth: 1,
                      borderRadius: BorderRadius.circular(8),
                      fieldHeight: 47,
                      fieldWidth: 47,
                      activeFillColor: AppColors.accentRed,
                    ),
                    animationDuration: const Duration(milliseconds: 300),
                    // backgroundColor: Colors.blue.shade50,
                    enableActiveFill: false,
                    controller: pincodeController,
                    onCompleted: (v) {
                      print("Completed");
                    },
                    onChanged: (value) {
                      // setState(() {
                      //   currentText = value;
                      // });
                    },
                  ),
                ),
              ],
            ),
            const Spacer(),
            SizedBox(
              width: double.infinity,
              height: 47,
              child: ElevatedButton(
                onPressed: () {
                  // context.router.push();
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: AppColors.accentGreen,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
                child: const Text(
                  "Далее",
                  style: AppFonts.s14W500,
                ),
              ),
            ),
            const SizedBox(height: 20),
          ],
        ),
      ),
    );
  }
}
