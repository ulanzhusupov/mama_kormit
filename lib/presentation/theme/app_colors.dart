import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color accentRed = Color(0xffD33E3E);
  static const Color accentRedButton = Color(0xffEE5D5D);
  static const Color accentGreen = Color(0xff3ACE63);
  static const Color textGrey = Color(0xff8C8C8C);
  static const Color searchFieldColor = Color(0xffF5F5F5);
  static const Color white = Colors.white;
  static const Color searchText = Color(0xff2F0B09);
  static const Color yellowDiscount = Color(0xffF1B927);
}
