import 'package:flutter/material.dart';

abstract class AppFonts {
  static const TextStyle s13W600 =
      TextStyle(fontSize: 13, fontWeight: FontWeight.w600);

  static const TextStyle s18W600 =
      TextStyle(fontSize: 18, fontWeight: FontWeight.w600);

  static const TextStyle s25W600 =
      TextStyle(fontSize: 25, fontWeight: FontWeight.w600);

  static const TextStyle s20W600 =
      TextStyle(fontSize: 20, fontWeight: FontWeight.w600);

  static const TextStyle s14W500 =
      TextStyle(fontSize: 14, fontWeight: FontWeight.w500);
  static const TextStyle s14W600 =
      TextStyle(fontSize: 14, fontWeight: FontWeight.w600);

  static const TextStyle s13W500 =
      TextStyle(fontSize: 13, fontWeight: FontWeight.w500);

  static const TextStyle s11W500 =
      TextStyle(fontSize: 11, fontWeight: FontWeight.w500);

  static const TextStyle s11W400 =
      TextStyle(fontSize: 11, fontWeight: FontWeight.w400);

  static const TextStyle s10W400 =
      TextStyle(fontSize: 10, fontWeight: FontWeight.w400);
}
