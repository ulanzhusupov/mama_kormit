import 'package:flutter/material.dart';
import 'package:mama_kormit/presentation/theme/app_colors.dart';
import 'package:mama_kormit/presentation/theme/app_fonts.dart';
import 'package:mama_kormit/presentation/widgets/filter_button.dart';
import 'package:mama_kormit/presentation/widgets/search_field.dart';
import 'package:mama_kormit/resources/resources.dart';

class MamaKormitAppBar extends StatelessWidget {
  const MamaKormitAppBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            spreadRadius: 0,
            blurRadius: 40,
            offset: const Offset(0, 0), // changes position of shadow
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InkWell(
                  onTap: () {},
                  child: Row(
                    children: [
                      const Icon(
                        Icons.location_on_outlined,
                        color: AppColors.accentRed,
                        size: 21,
                      ),
                      const SizedBox(
                        width: 4,
                      ),
                      Text(
                        "г. Казань",
                        style: AppFonts.s14W600.copyWith(
                          color: AppColors.accentRed,
                        ),
                      ),
                    ],
                  ),
                ),
                Image.asset(
                  Images.logo,
                  width: MediaQuery.of(context).size.width * 0.45,
                ),
                Row(
                  children: [
                    Image.asset(
                      Images.heart,
                      width: 22,
                    ),
                    CircleAvatar(
                      backgroundColor: AppColors.accentRed,
                      radius: 12.5,
                      child: Text(
                        "22",
                        style: AppFonts.s11W500.copyWith(
                          color: AppColors.white,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 8),
            const Row(
              children: [
                SearchField(),
                SizedBox(width: 6),
                FilterButton(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
