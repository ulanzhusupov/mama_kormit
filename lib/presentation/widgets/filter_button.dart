import 'package:flutter/material.dart';
import 'package:mama_kormit/presentation/theme/app_colors.dart';

class FilterButton extends StatelessWidget {
  const FilterButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 47,
      child: ElevatedButton(
        onPressed: () {},
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
          backgroundColor: AppColors.accentRed,
        ),
        child: const Icon(
          Icons.tune_outlined,
          color: AppColors.white,
        ),
      ),
    );
  }
}
