import 'package:flutter/material.dart';
import 'package:mama_kormit/presentation/theme/app_colors.dart';
import 'package:mama_kormit/presentation/theme/app_fonts.dart';

class StreetLocation extends StatelessWidget {
  const StreetLocation({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.1),
            spreadRadius: 0,
            blurRadius: 40,
            offset: const Offset(0, 0), // changes position of shadow
          ),
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 19, horizontal: 23),
        child: Row(
          children: [
            const Icon(
              Icons.location_on_outlined,
              color: AppColors.accentRed,
            ),
            Text(
              "ул. Правобережная д. 23",
              style: AppFonts.s14W500.copyWith(color: AppColors.accentRed),
            ),
          ],
        ),
      ),
    );
  }
}
