import 'package:flutter/material.dart';
import 'package:mama_kormit/presentation/theme/app_colors.dart';
import 'package:mama_kormit/presentation/theme/app_fonts.dart';
import 'package:mama_kormit/resources/resources.dart';

class SearchField extends StatelessWidget {
  const SearchField({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.68,
      height: 47,
      child: TextField(
        textAlignVertical: TextAlignVertical.center,
        style: AppFonts.s14W500.copyWith(
          color: AppColors.searchText,
        ),
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.only(
            top: 60,
            left: 10,
          ),
          hintText: "Найти продукт",
          hintStyle: AppFonts.s14W500.copyWith(
            color: AppColors.searchText,
          ),
          filled: true,
          fillColor: AppColors.searchFieldColor,
          prefix: Padding(
            padding: const EdgeInsets.only(
              right: 10,
              top: 30,
            ),
            child: Image.asset(
              Images.search,
              width: 25,
            ),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(8),
          ),
        ),
      ),
    );
  }
}
