import 'package:flutter/material.dart';
import 'package:mama_kormit/presentation/theme/app_colors.dart';
import 'package:mama_kormit/presentation/theme/app_fonts.dart';
import 'package:mama_kormit/resources/resources.dart';

class BonusProgramCard extends StatelessWidget {
  const BonusProgramCard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 175,
      decoration: BoxDecoration(
        color: AppColors.accentRed,
        borderRadius: BorderRadius.circular(8),
        image: const DecorationImage(
          image: AssetImage(Images.bonusBg),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 26),
          Text(
            "Бонусная программа",
            style: AppFonts.s20W600.copyWith(
              color: AppColors.white,
            ),
          ),
          const SizedBox(height: 9),
          Text(
            "Пройди регистрацию и стань участником бонусной программы",
            style: AppFonts.s13W500.copyWith(
              color: AppColors.white,
            ),
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 9),
          ElevatedButton(
            onPressed: () {},
            style: ElevatedButton.styleFrom(
                backgroundColor: AppColors.accentRedButton,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                )),
            child: Text(
              "Войти / Регистрация",
              style: AppFonts.s13W600.copyWith(
                color: AppColors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
