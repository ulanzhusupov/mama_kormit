import 'package:flutter/material.dart';
import 'package:mama_kormit/core/custom_screen_util.dart';
import 'package:mama_kormit/presentation/theme/app_colors.dart';
import 'package:mama_kormit/presentation/theme/app_fonts.dart';

class NewsTile extends StatelessWidget {
  const NewsTile({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: CustomScreenUtil.width(context, 326),
      height: CustomScreenUtil.height(context, 200),
      decoration: BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.circular(8),
        boxShadow: const [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.1),
            blurRadius: 40,
          ),
        ],
      ),
      child: Row(
        children: [
          Container(
            width: CustomScreenUtil.width(context, 116),
            decoration: const BoxDecoration(
              color: AppColors.accentRed,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8),
                bottomLeft: Radius.circular(8),
              ),
            ),
          ),
          SizedBox(
            width: CustomScreenUtil.width(context, 210),
            child: Padding(
              padding: EdgeInsets.only(
                left: CustomScreenUtil.width(context, 20),
                top: CustomScreenUtil.height(context, 19),
                right: CustomScreenUtil.width(context, 14),
                bottom: CustomScreenUtil.height(context, 17),
              ),
              child: Column(
                children: [
                  const Text(
                    "У нас новое поступление",
                    style: AppFonts.s13W500,
                  ),
                  SizedBox(height: CustomScreenUtil.width(context, 4)),
                  Row(
                    children: [
                      const Icon(
                        Icons.calendar_month_outlined,
                        color: AppColors.accentRed,
                        size: 15,
                      ),
                      const SizedBox(width: 3),
                      Text(
                        "03.01.2023",
                        style: AppFonts.s10W400.copyWith(
                          color: AppColors.accentRed,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: CustomScreenUtil.height(context, 15)),
                  Text(
                    "Пельмени \"Уральские\" - это замороженное блюдо, которое готовят из пельменного теста с начинкой из те..",
                    style: AppFonts.s11W400.copyWith(color: AppColors.textGrey),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
