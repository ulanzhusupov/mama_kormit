import 'package:flutter/material.dart';
import 'package:mama_kormit/presentation/theme/app_colors.dart';
import 'package:mama_kormit/presentation/theme/app_fonts.dart';
import 'package:mama_kormit/resources/resources.dart';

class TitleAndViewAllSection extends StatelessWidget {
  const TitleAndViewAllSection({
    super.key,
    required this.title,
  });

  final String title;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: AppFonts.s18W600,
        ),
        InkWell(
          onTap: () {},
          child: Row(
            children: [
              Text(
                "Смотреть все",
                style: AppFonts.s13W500.copyWith(color: AppColors.accentRed),
              ),
              const SizedBox(width: 2),
              Image.asset(
                Images.arrowRightCircle,
                width: 24,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
